﻿using QyA_Demo.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QyA_Demo
{
    public partial class MainForm : Form
    {

        QyA quiet;
        QyAReadFlags readFlags = (QyAReadFlags)0x1F;


        public MainForm()
        {
            BaseInitialize();
        }

        public MainForm(QyA quiet)
        {
            BaseInitialize();

            this.quiet = new QyA(quiet, QyAComType.USART);
        }

        private void BaseInitialize()
        {
            InitializeComponent();

            digitalOutputDisplay.Text = "Digital Outputs";

            RedrawAnalogOutputs(2);
    }

        private void RedrawAnalogOutputs(int Count)
        {
            for (int count = anaOutputTableLayoutPanel.Controls.Count - 1; count >= 0; count--)
            {
                AnalogDisplay oldDisplay = (AnalogDisplay)anaOutputTableLayoutPanel.Controls[count];

                oldDisplay.ValueChanged -= AnalogOutput_ValueChanged;
                oldDisplay.Dispose();
            }

            anaOutputTableLayoutPanel.Controls.Clear();
            anaOutputTableLayoutPanel.RowCount = Count;
            anaOutputTableLayoutPanel.RowStyles.Clear();

            float rowPercent = 1F / Count;

            for (int count = 0; count < Count; count++)
            {
                AnalogDisplay newDis = new AnalogDisplay
                {
                    ChannelNumber = count + 1,
                    EnableInput = true,
                    Maximum = 3.3M,
                    Minimum = 0M,
                    Value = 0,
                    Name = $"anaOutputDisplay{count + 1}",
                    Dock = DockStyle.Fill,
                };

                newDis.ValueChanged += AnalogOutput_ValueChanged;
                anaOutputTableLayoutPanel.Controls.Add(newDis, 0, count);

                anaOutputTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, rowPercent));
            }

            float topPercent = 5F / (5F + Count);
            mainTableLayoutPanel.RowStyles[0].Height = topPercent;
            mainTableLayoutPanel.RowStyles[1].Height = 1F - topPercent;
        }

        Timer anaOutTimer;

        private void AnalogOutput_ValueChanged(object sender, EventArgs e)
        {
            if (anaOutTimer == null)
            {
                anaOutTimer = new Timer();
                //anaOutTimer.Tick += AnaOutTimer_Tick;
            }

            AnalogDisplay anaDisplay = (AnalogDisplay)sender;

            quiet?.WriteToAnalogOutput(anaDisplay.ChannelNumber - 1,
                (int)(anaDisplay.Value * 310));
        }

        private void AnaOutTimer_Tick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (quiet == null)
            {
                List<string> ports = SerialPort.GetPortNames().ToList();

                ports.Remove("COM1");

                if (ports.Count() > 0)
                {
                    SerialPort serial = new SerialPort(ports.First());
                    quiet = new QyA(serial);

                    serial.Open();

                    dataTimer.Start();

                    digitalOutputDisplay.ValueChanged += DigitalOutputDisplay_ValueChanged;

                    nameLabel.Text = quiet.GetBoardName();
                }
            }
            else
            {
                dataTimer.Start();

                digitalOutputDisplay.ValueChanged += DigitalOutputDisplay_ValueChanged;

                nameLabel.Text = quiet.GetBoardName();
            }
        }


        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingFrom settingFrom = new SettingFrom(quiet);

            settingFrom.ShowDialog();
        }

        private void DataTimer_Tick(object sender, EventArgs e)
        {
            QyAData readData = quiet.QueryData(readFlags);

            digitalInputDisplay.Value = readData.Digitals;

            foreach (AnalogDisplay anaDisplay in anaInputtableLayoutPanel.Controls.OfType<AnalogDisplay>())
                anaDisplay.Value = (readData.Analogs[anaDisplay.ChannelNumber - 1] / 310M);

        }

        private void DigitalOutputDisplay_ValueChanged(object sender, EventArgs e)
        {
            quiet.WriteToDigitalOutputs((byte)digitalOutputDisplay.Value);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MainForm subForm = new MainForm(quiet);

            subForm.Show();
        }
    }
}
