﻿using QyA_Demo.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QyA_Demo
{
    public partial class SettingFrom : Form
    {
        byte[] SettingsBytes;

        QyA quiet;

        public SettingFrom()
        {
            InitializeComponent();
        }

        public SettingFrom(QyA quiet)
        {
            InitializeComponent();

            this.quiet = quiet;

            controlPanelScrollBar.Scroll += (sender, e) => { controlPanel.VerticalScroll.Value = controlPanelScrollBar.Value; };

            nameLabel.Text = $"Name: {quiet.GetBoardName()}";
            serialLabel.Text = $"SN: {quiet.GetSerialNumber()}";
            firmwareLabel.Text = $"FID: {quiet.GetFirmwareID()}";

            SettingsBytes = quiet.ReadAllSettings();

            string descript = quiet.ReadSettingsDescription();
            List<string> description = descript.Split('\n').ToList();

            description.RemoveAt(0);
            description.RemoveAt(description.Count - 1);

            Stack<TreeNode> activeTree = new Stack<TreeNode>();

            foreach (string line in description)
            {
                int tabLevel = line.Count(ch => ch == '\t');

                List<string> splitLine = line.Trim().Split('=').ToList();

                if (splitLine.Count() < 2)
                {
                    splitLine.Add("Err");   //Mark this item as an error
#if DEBUG
                    MessageBox.Show($"{line.Trim()} did not contain a size!", "Developer Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
#endif
                }

                if (char.IsDigit(line[tabLevel]))
                {
                    int skipCount = int.Parse(line.Trim());
                    continue;
                }

                SettingsNode newNode = new SettingsNode(splitLine[1])
                {
                    Text = splitLine[0],
                    Tag = splitLine[1],
                };

                if (tabLevel == 0)
                {
                    settingsTreeView.Nodes.Add(newNode);

                    activeTree.Clear();
                    activeTree.Push(newNode);
                }
                else
                {
                    int depth = activeTree.Count - 1;

                    if (depth == tabLevel)
                    {
                        if (activeTree.Peek().Parent == null)
                            settingsTreeView.Nodes.Add(newNode);
                        else
                            activeTree.Peek().Parent.Nodes.Add(newNode);

                        activeTree.Pop();

                        activeTree.Push(newNode);
                    }
                    else if (depth < tabLevel)
                    {
                        activeTree.Peek().Nodes.Add(newNode);
                        activeTree.Push(newNode);
                    }
                    else
                    {
                        while (activeTree.Count > tabLevel + 1)
                            activeTree.Pop();

                        if (activeTree.Peek().Parent == null)
                            settingsTreeView.Nodes.Add(newNode);
                        else
                            activeTree.Peek().Parent.Nodes.Add(newNode);

                        activeTree.Pop();
                        activeTree.Push(newNode);
                    }
                }
            }

            int Offset = 0;
            foreach (SettingsNode node in settingsTreeView.Nodes)
            {
                Offset = CalculateSettingsLocation(node, Offset);
            }
        }

        private int CalculateSettingsLocation(SettingsNode node, int byteOffset)
        {
            node.ByteLocation = byteOffset;

            int childOffset = byteOffset;
            int bitOffset = 0;
            foreach (SettingsNode childNode in node.Nodes)
            {
                if (childNode.DataType == SettingDataType.Bit)
                {
                    childOffset = CalculateSettingsLocation(childNode, childOffset);
                    childNode.BitLocation = bitOffset++;
                }
                else
                    childOffset = CalculateSettingsLocation(childNode, childOffset);
            }

            return byteOffset + node.ByteCount;
        }

        private GroupBox CreateGroupBox(string Text)
        {
            return new GroupBox
            {
                Dock = DockStyle.Fill,
                Text = Text,
            };
        }

        private CheckBox CreateCheckBox(string Text)
        {
            return new CheckBox
            {
                Dock = DockStyle.Fill,
                Text = Text,
            };
        }


        private void valueChanged(object sender, EventArgs e)
        {
            activeNode.ForeColor = Color.Red;
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            if (activeNode != null && activeNode.ForeColor == Color.Red)
                ApplySettings();
        }

        private void ApplySettings()
        {
            foreach (Control control in controlPanel.Controls)
            {
                if (control.Tag is SettingsNode controlNode)
                {
                    if (control is CheckBox checkBox)
                    {
                        int bitMask = 1 << controlNode.BitLocation;
                        if (checkBox.Checked)
                            SettingsBytes[controlNode.ByteLocation] |= (byte)bitMask;
                        else
                            SettingsBytes[controlNode.ByteLocation] &= (byte)~bitMask;
                    }
                    else if (control is NumericUpDown numericUpDown)
                    {
                        SettingsBytes[controlNode.ByteCount] = (byte)((int)numericUpDown.Value & 0xFF);

                        if (controlNode.DataType == SettingDataType.Int)
                            SettingsBytes[controlNode.ByteLocation + 1] = (byte)((int)numericUpDown.Value >> 8);
                    }
                    else if (control is TextBox textBox)
                    {
                        for (int i = 0; i < textBox.MaxLength; i++)
                        {
                            if (i < textBox.TextLength)
                                SettingsBytes[controlNode.ByteLocation + i] = Convert.ToByte(textBox.Text[i]);
                            else
                                SettingsBytes[controlNode.ByteLocation + i] = 0x00;
                        }
                    }
                    else
                    {
#if DEBUG
                        MessageBox.Show("Unknown Control Type?", "Developer Error",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
#endif
                    }
                }
            }

            activeNode.ForeColor = Color.Black;
        }

        SettingsNode activeNode;
        private void settingsTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (activeNode != null && activeNode.ForeColor == Color.Red)
            {
                if (DialogResult.Yes == MessageBox.Show("Unsaved settings are displayed.\nApply Settings?",
                    "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    ApplySettings();
                }
            }
            activeNode = (SettingsNode)settingsTreeView.SelectedNode;

            //Remove all old controls (except the scroll bar)
            for (int count = controlPanel.Controls.Count - 1; count >= 0; count--)
                controlPanel.Controls[count].Dispose();

            //if (activeNode.DataType == null)
            //{
            //    Label errLabel = new Label
            //    {
            //        Text = $"Error Loading {activeNode.Text}",
            //        Dock = DockStyle.Fill,
            //        TextAlign = ContentAlignment.MiddleCenter,
            //    };

            //    controlPanel.Controls.Add(errLabel);
            //}

            if (activeNode.Nodes.Count > 0)
            {
                foreach (SettingsNode node in activeNode.Nodes)
                    CreateControlFromNode(node, node.Index);
            }
            else
            {
                for (int count = 0; count < activeNode.DataCount; count++)
                {
                    CreateControlFromNode(activeNode, count);

                    //If the DataType is Char, don't do more
                    if (((string)activeNode.Tag)[0] == 'C')
                        break;
                }
            }
        }

        private void CreateControlFromNode(SettingsNode node, int Row)
        {
            switch (node.DataType)
            {
                case SettingDataType.Bit:
                    AddLabel(node);

                    int bitMask = 1 << node.BitLocation;

                    CheckBox checkBox = new CheckBox
                    {
                        Dock = DockStyle.Fill,
                        CheckAlign = ContentAlignment.TopLeft,
                        Checked = (SettingsBytes[node.ByteLocation] & bitMask) > 0,
                        Tag = node,
                    };

                    checkBox.CheckedChanged += valueChanged;
                    controlPanel.Controls.Add(checkBox);

                    break;
                case SettingDataType.Byte:
                    AddNumericInput(node, byte.MaxValue, node.DataCount);
                    break;
                case SettingDataType.Char:
                    AddLabel(node);

                    TextBox textBox = new TextBox
                    {
                        Text = Encoding.Default.GetString(SettingsBytes,
                        activeNode.ByteLocation, activeNode.DataCount),
                        MaxLength = node.DataCount,
                        MaximumSize = new Size(120, 24),
                        Dock = DockStyle.Fill,
                        Tag = node,
                    };

                    textBox.TextChanged += valueChanged;

                    controlPanel.Controls.Add(textBox);
                    break;
                case SettingDataType.Int:
                    AddNumericInput(node, uint.MaxValue, node.DataCount);
                    break;
            }
        }

        private void AddLabel(TreeNode node)
        {
            AddLabel(node.Text);
        }

        private void AddLabel(string Text)
        {
            controlPanel.Controls.Add(new Label
            {
                Text = Text,
                Dock = DockStyle.Fill,
                TextAlign = ContentAlignment.TopRight,
                Padding = new Padding(6),
            });
        }

        private void AddNumericInput(SettingsNode node, long Max, int Size)
        {
            for (int count = 0; count < Size; count++)
            {
                if (Size <= 1)
                    AddLabel(node.Text);
                else
                    AddLabel($"{node.Text}[{count}]");

                int value = SettingsBytes[node.ByteLocation];

                if (node.DataType == SettingDataType.Int)
                    value += SettingsBytes[node.ByteLocation + 1] << 8;

                NumericUpDown numericUpDown = new NumericUpDown
                {
                    Maximum = Max,
                    Minimum = 0,
                    DecimalPlaces = 0,
                    Value = value,
                    Tag = node,
                };

                numericUpDown.ValueChanged += valueChanged;

                controlPanel.Controls.Add(numericUpDown);
            }
        }

        private void commitButton_Click(object sender, EventArgs e)
        {
            quiet.WriteAllSettings(SettingsBytes);
            quiet.CommitSettings();
        }
    }
}
