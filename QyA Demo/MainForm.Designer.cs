﻿namespace QyA_Demo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.analogInputGroupBox = new System.Windows.Forms.GroupBox();
            this.anaInputtableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.analogInDisplay4 = new QyA_Demo.Controls.AnalogDisplay();
            this.analogInDisplay3 = new QyA_Demo.Controls.AnalogDisplay();
            this.analogInDisplay2 = new QyA_Demo.Controls.AnalogDisplay();
            this.analogInDisplay1 = new QyA_Demo.Controls.AnalogDisplay();
            this.digitalInputDisplay = new QyA_Demo.DigitalDisplay();
            this.digitalOutputDisplay = new QyA_Demo.DigitalDisplay();
            this.anaOutputGroupBox = new System.Windows.Forms.GroupBox();
            this.anaOutputTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.analogOutDisplay1 = new QyA_Demo.Controls.AnalogDisplay();
            this.analogOutDisplay2 = new QyA_Demo.Controls.AnalogDisplay();
            this.button1 = new System.Windows.Forms.Button();
            this.nameLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataTimer = new System.Windows.Forms.Timer(this.components);
            this.mainTableLayoutPanel.SuspendLayout();
            this.analogInputGroupBox.SuspendLayout();
            this.anaInputtableLayoutPanel.SuspendLayout();
            this.anaOutputGroupBox.SuspendLayout();
            this.anaOutputTableLayoutPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.ColumnCount = 3;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.mainTableLayoutPanel.Controls.Add(this.analogInputGroupBox, 1, 0);
            this.mainTableLayoutPanel.Controls.Add(this.digitalInputDisplay, 0, 0);
            this.mainTableLayoutPanel.Controls.Add(this.digitalOutputDisplay, 2, 0);
            this.mainTableLayoutPanel.Controls.Add(this.anaOutputGroupBox, 1, 1);
            this.mainTableLayoutPanel.Controls.Add(this.button1, 2, 2);
            this.mainTableLayoutPanel.Controls.Add(this.nameLabel, 0, 2);
            this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 24);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 3;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(543, 268);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // analogInputGroupBox
            // 
            this.analogInputGroupBox.Controls.Add(this.anaInputtableLayoutPanel);
            this.analogInputGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analogInputGroupBox.Location = new System.Drawing.Point(111, 3);
            this.analogInputGroupBox.Name = "analogInputGroupBox";
            this.analogInputGroupBox.Size = new System.Drawing.Size(319, 156);
            this.analogInputGroupBox.TabIndex = 2;
            this.analogInputGroupBox.TabStop = false;
            this.analogInputGroupBox.Text = "Analog Inputs";
            // 
            // anaInputtableLayoutPanel
            // 
            this.anaInputtableLayoutPanel.ColumnCount = 1;
            this.anaInputtableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.anaInputtableLayoutPanel.Controls.Add(this.analogInDisplay4, 0, 3);
            this.anaInputtableLayoutPanel.Controls.Add(this.analogInDisplay3, 0, 2);
            this.anaInputtableLayoutPanel.Controls.Add(this.analogInDisplay2, 0, 1);
            this.anaInputtableLayoutPanel.Controls.Add(this.analogInDisplay1, 0, 0);
            this.anaInputtableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.anaInputtableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.anaInputtableLayoutPanel.Name = "anaInputtableLayoutPanel";
            this.anaInputtableLayoutPanel.RowCount = 4;
            this.anaInputtableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.anaInputtableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.anaInputtableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.anaInputtableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.anaInputtableLayoutPanel.Size = new System.Drawing.Size(313, 137);
            this.anaInputtableLayoutPanel.TabIndex = 1;
            // 
            // analogInDisplay4
            // 
            this.analogInDisplay4.ChannelNumber = 4;
            this.analogInDisplay4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analogInDisplay4.Location = new System.Drawing.Point(3, 105);
            this.analogInDisplay4.Maximum = new decimal(new int[] {
            33,
            0,
            0,
            65536});
            this.analogInDisplay4.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.analogInDisplay4.Name = "analogInDisplay4";
            this.analogInDisplay4.Size = new System.Drawing.Size(307, 29);
            this.analogInDisplay4.TabIndex = 3;
            this.analogInDisplay4.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // analogInDisplay3
            // 
            this.analogInDisplay3.ChannelNumber = 3;
            this.analogInDisplay3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analogInDisplay3.Location = new System.Drawing.Point(3, 71);
            this.analogInDisplay3.Maximum = new decimal(new int[] {
            33,
            0,
            0,
            65536});
            this.analogInDisplay3.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.analogInDisplay3.Name = "analogInDisplay3";
            this.analogInDisplay3.Size = new System.Drawing.Size(307, 28);
            this.analogInDisplay3.TabIndex = 2;
            this.analogInDisplay3.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // analogInDisplay2
            // 
            this.analogInDisplay2.ChannelNumber = 2;
            this.analogInDisplay2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analogInDisplay2.Location = new System.Drawing.Point(3, 37);
            this.analogInDisplay2.Maximum = new decimal(new int[] {
            33,
            0,
            0,
            65536});
            this.analogInDisplay2.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.analogInDisplay2.Name = "analogInDisplay2";
            this.analogInDisplay2.Size = new System.Drawing.Size(307, 28);
            this.analogInDisplay2.TabIndex = 1;
            this.analogInDisplay2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // analogInDisplay1
            // 
            this.analogInDisplay1.ChannelNumber = 1;
            this.analogInDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analogInDisplay1.Location = new System.Drawing.Point(3, 3);
            this.analogInDisplay1.Maximum = new decimal(new int[] {
            33,
            0,
            0,
            65536});
            this.analogInDisplay1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.analogInDisplay1.Name = "analogInDisplay1";
            this.analogInDisplay1.Size = new System.Drawing.Size(307, 28);
            this.analogInDisplay1.TabIndex = 0;
            this.analogInDisplay1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // digitalInputDisplay
            // 
            this.digitalInputDisplay.Bits = 8;
            this.digitalInputDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.digitalInputDisplay.Location = new System.Drawing.Point(3, 3);
            this.digitalInputDisplay.Name = "digitalInputDisplay";
            this.mainTableLayoutPanel.SetRowSpan(this.digitalInputDisplay, 2);
            this.digitalInputDisplay.Size = new System.Drawing.Size(102, 237);
            this.digitalInputDisplay.TabIndex = 3;
            // 
            // digitalOutputDisplay
            // 
            this.digitalOutputDisplay.Bits = 8;
            this.digitalOutputDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.digitalOutputDisplay.EnableToggling = true;
            this.digitalOutputDisplay.Location = new System.Drawing.Point(436, 3);
            this.digitalOutputDisplay.Name = "digitalOutputDisplay";
            this.mainTableLayoutPanel.SetRowSpan(this.digitalOutputDisplay, 2);
            this.digitalOutputDisplay.Size = new System.Drawing.Size(104, 237);
            this.digitalOutputDisplay.TabIndex = 4;
            // 
            // anaOutputGroupBox
            // 
            this.anaOutputGroupBox.Controls.Add(this.anaOutputTableLayoutPanel);
            this.anaOutputGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.anaOutputGroupBox.Location = new System.Drawing.Point(111, 165);
            this.anaOutputGroupBox.Name = "anaOutputGroupBox";
            this.mainTableLayoutPanel.SetRowSpan(this.anaOutputGroupBox, 2);
            this.anaOutputGroupBox.Size = new System.Drawing.Size(319, 100);
            this.anaOutputGroupBox.TabIndex = 0;
            this.anaOutputGroupBox.TabStop = false;
            this.anaOutputGroupBox.Text = "Analog Outputs";
            // 
            // anaOutputTableLayoutPanel
            // 
            this.anaOutputTableLayoutPanel.ColumnCount = 1;
            this.anaOutputTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.anaOutputTableLayoutPanel.Controls.Add(this.analogOutDisplay1, 0, 0);
            this.anaOutputTableLayoutPanel.Controls.Add(this.analogOutDisplay2, 0, 1);
            this.anaOutputTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.anaOutputTableLayoutPanel.Location = new System.Drawing.Point(3, 16);
            this.anaOutputTableLayoutPanel.Name = "anaOutputTableLayoutPanel";
            this.anaOutputTableLayoutPanel.RowCount = 2;
            this.anaOutputTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.anaOutputTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.anaOutputTableLayoutPanel.Size = new System.Drawing.Size(313, 81);
            this.anaOutputTableLayoutPanel.TabIndex = 5;
            // 
            // analogOutDisplay1
            // 
            this.analogOutDisplay1.ChannelNumber = 1;
            this.analogOutDisplay1.EnableInput = true;
            this.analogOutDisplay1.Location = new System.Drawing.Point(3, 3);
            this.analogOutDisplay1.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.analogOutDisplay1.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.analogOutDisplay1.Name = "analogOutDisplay1";
            this.analogOutDisplay1.Size = new System.Drawing.Size(307, 31);
            this.analogOutDisplay1.TabIndex = 0;
            this.analogOutDisplay1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // analogOutDisplay2
            // 
            this.analogOutDisplay2.ChannelNumber = 2;
            this.analogOutDisplay2.EnableInput = true;
            this.analogOutDisplay2.Location = new System.Drawing.Point(3, 43);
            this.analogOutDisplay2.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.analogOutDisplay2.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.analogOutDisplay2.Name = "analogOutDisplay2";
            this.analogOutDisplay2.Size = new System.Drawing.Size(307, 31);
            this.analogOutDisplay2.TabIndex = 1;
            this.analogOutDisplay2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(436, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 19);
            this.button1.TabIndex = 5;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(3, 243);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(102, 25);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "Name";
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(543, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.toolStripSeparator1,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(113, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // dataTimer
            // 
            this.dataTimer.Tick += new System.EventHandler(this.DataTimer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 292);
            this.Controls.Add(this.mainTableLayoutPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Qy@ Demo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.mainTableLayoutPanel.PerformLayout();
            this.analogInputGroupBox.ResumeLayout(false);
            this.anaInputtableLayoutPanel.ResumeLayout(false);
            this.anaOutputGroupBox.ResumeLayout(false);
            this.anaOutputTableLayoutPanel.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.GroupBox analogInputGroupBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private DigitalDisplay digitalInputDisplay;
        private DigitalDisplay digitalOutputDisplay;
        private System.Windows.Forms.Timer dataTimer;
        private Controls.AnalogDisplay analogInDisplay1;
        private System.Windows.Forms.TableLayoutPanel anaInputtableLayoutPanel;
        private Controls.AnalogDisplay analogInDisplay4;
        private Controls.AnalogDisplay analogInDisplay3;
        private Controls.AnalogDisplay analogInDisplay2;
        private System.Windows.Forms.TableLayoutPanel anaOutputTableLayoutPanel;
        private System.Windows.Forms.GroupBox anaOutputGroupBox;
        private Controls.AnalogDisplay analogOutDisplay1;
        private Controls.AnalogDisplay analogOutDisplay2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label nameLabel;
    }
}

