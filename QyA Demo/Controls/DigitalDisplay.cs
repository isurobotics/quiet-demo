﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QyA_Demo
{
    public partial class DigitalDisplay : UserControl
    {
        string _text;
        [Browsable(true)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public new string Text
        {
            get => groupBox.Text;
            set
            {
                groupBox.Text = value;
                _text = value;
            }
        }

        int _value;
        [Description("The Displayed Value")]
        [DefaultValue(0)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public int Value
        {
            get => _value;

            set
            {
                _value = value;

                foreach(PictureBox pictureBox in tableLayoutPanel.Controls.OfType<PictureBox>())
                {
                    if ((_value & (int)pictureBox.Tag) > 0)
                        pictureBox.BackColor = Color.LawnGreen;
                    else
                        pictureBox.BackColor = Color.DarkGray;
                }

                ValueChanged?.Invoke(this, null);
            }
        }

        int _bits;
        /// <summary>
        /// The number of bits to display
        /// </summary>
        [Description("The number of bits to display")]
        [DefaultValue(1)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public int Bits
        {
            get => _bits;

            set
            {
                _bits = value < 1 ? 1 : value;

                tableLayoutPanel.RowCount = Bits;

                tableLayoutPanel.Controls.Clear();

                float rowPercent = 1F / _bits;
                for (int count = 0; count < Bits; count++)
                {
                    tableLayoutPanel.Controls.Add(CreateLabel(count.ToString()), 0, count);
                    tableLayoutPanel.Controls.Add(CreateDigPictureBox(count), 1, count);

                    tableLayoutPanel.RowStyles[count].SizeType = SizeType.Percent;
                    tableLayoutPanel.RowStyles[count].Height = rowPercent;
                }
            }
        }

        bool _enableToggling;
        /// <summary>
        /// Enable toggling the indicators by clicking them
        /// </summary>
        [Description("Enable toggling the indicators by clicking them")]
        [DefaultValue(false)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public bool EnableToggling
        {
            get => _enableToggling;

            set
            {
                _enableToggling = value;

                foreach (PictureBox pictureBox in tableLayoutPanel.Controls.OfType<PictureBox>())
                    pictureBox.Click -= PictureBox_Click;

                if (value)
                    foreach (PictureBox pictureBox in tableLayoutPanel.Controls.OfType<PictureBox>())
                        pictureBox.Click += PictureBox_Click;
            }
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            PictureBox pictureBox = (PictureBox)sender;

            Value = Value ^ (int)pictureBox.Tag;
        }

        public DigitalDisplay()
        {
            InitializeComponent();
        }

        public event EventHandler ValueChanged;

        private Label CreateLabel(string Text)
        {
            return new Label
            {
                AutoSize = true,
                Dock = DockStyle.Right,
                Name = $"{Text}Label",
                Size = new Size(13, 200),
                TabIndex = 0,
                Text = Text,
                TextAlign = ContentAlignment.MiddleRight
            };
        }

        private PictureBox CreateDigPictureBox(int number)
        {
            int weight = (int)Math.Pow(2, number);

            return new PictureBox
            {
                BackColor = SystemColors.ControlDark,
                BorderStyle = BorderStyle.Fixed3D,
                Dock = DockStyle.Fill,
                Size = new Size(60, 194),
                TabIndex = 1,
                TabStop = false,
                Tag = weight
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Bits">The number of bits that will be represented</param>
        public DigitalDisplay(int Bits)
        {
            this.Bits = Bits;
        }
    }
}
