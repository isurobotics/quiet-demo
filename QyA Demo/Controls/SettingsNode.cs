﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QyA_Demo.Controls
{

    public enum SettingDataType
    {
        Bit,
        Byte,
        Char,
        Int,
    }

    public class SettingsNode : TreeNode
    {
        public int ByteLocation { get; set; }
        public int BitLocation { get; set; }

        public readonly SettingDataType DataType;
        public readonly int DataCount;

        public int ByteCount => BitCount / 8;
        public readonly int BitCount;

        public SettingsNode(string typeString)
        {
            DataCount = int.Parse(typeString.Substring(1));

            switch (typeString[0])
            {
                case 'b':
                    BitCount = 1;
                    DataType = SettingDataType.Bit;
                    break;
                case 'B':
                    BitCount = 8;
                    DataType = SettingDataType.Byte;
                    break;
                case 'C':
                    BitCount = 8;
                    DataType = SettingDataType.Char;
                    break;
                case 'I':
                    BitCount = 16;
                    DataType = SettingDataType.Int;
                    break;
            }

            BitCount *= DataCount;
        }
    }
}
