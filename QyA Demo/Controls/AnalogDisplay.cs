﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Design;

namespace QyA_Demo.Controls
{
    public partial class AnalogDisplay : UserControl
    {
        bool _enableInput;

        [Description("Enable the user to change the value by holding down the mouse")]
        [DefaultValue(false)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public bool EnableInput
        {
            get => _enableInput;

            set
            {
                progressBar.MouseDown -= ProgressBar_MouseDown;
                progressBar.MouseMove -= ProgressBar_MouseDown;

                _enableInput = value;

                if (value)
                {
                    progressBar.SizeChanged += ProgressBar_SizeChanged;
                    progressBar.MouseMove += ProgressBar_MouseDown;
                }
            }
        }

        public event EventHandler ValueChanged;

        decimal _value;
        [RefreshProperties(RefreshProperties.Repaint)]
        public decimal Value
        {
            get => _value;

            set
            {
                if (value > Maximum)
                    _value = Maximum;
                else if (value < Minimum)
                    _value = Minimum;
                else
                    _value = value;

                decimal dValue = (value / Maximum) * progressBar.Maximum;

                if (dValue > progressBar.Maximum)
                    progressBar.Value = progressBar.Maximum;
                else if (dValue < progressBar.Minimum)
                    progressBar.Value = progressBar.Minimum;
                else
                    progressBar.Value = (int)dValue;

                valueLabel.Text = $"{_value.ToString("0.000")}{Units}";

                ValueChanged?.Invoke(this, null);
            }
        }

        [DefaultValue(100)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public decimal Maximum { get; set; } = 100;

        [DefaultValue(0)]
        [RefreshProperties(RefreshProperties.Repaint)]
        public decimal Minimum { get; set; } = 0;

        [DefaultValue("V")]
        [RefreshProperties(RefreshProperties.Repaint)]
        public string Units { get; set; } = "V";

        int _channelNumber = -1;
        [RefreshProperties(RefreshProperties.Repaint)]
        public int ChannelNumber
        {
            get
            {
                if (_channelNumber < 0)
                    return int.Parse(channelLabel.Text);
                else
                    return _channelNumber;
            }

            set
            {
                channelLabel.Text = value.ToString();
                _channelNumber = value;
            }

        }

        public AnalogDisplay()
        {
            InitializeComponent();
        }

        private void ProgressBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left == e.Button)
                Value = ((e.X / (decimal)progressBar.Width) * (Maximum - Minimum));
        }

        private void ProgressBar_SizeChanged(object sender, EventArgs e)
        {
            progressBar.Maximum = progressBar.Width;
        }


    }
}
