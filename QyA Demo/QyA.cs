﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QyA_Demo
{
    public class QyA
    {
        public const int AnalogInputCount = 4;
        public const int AnalogOutputCount = 10;

        public const int DigitalInputCount = 8;
        public const int DigitalOutputConut = 8;

        int _timeout = 500;
        public int Timeout
        {
            get => _timeout;
            set
            {
                _timeout = value;
                if (serialPort != null)
                    serialPort.ReadTimeout = Timeout;
            }
        }

        ICom Com = new ICom();
        SerialPort serialPort;

        public QyA(SerialPort serialPort)
        {
            this.serialPort = serialPort;

            Com.Read = new ICom.ReadStringDelegate(serialPort.ReadExisting);
            Com.ReadBytes = new ICom.ReadByteDelegate(serialPort.Read);
            Com.ReadTo = new ICom.ReadToDelagate(serialPort.ReadTo);

            Com.Write = new ICom.WriteDelage(serialPort.Write);
            Com.WriteBytes = new ICom.WriteByteDelegate(serialPort.Write);

            serialPort.ReadTimeout = Timeout;
            //serialPort.WriteTimeout = 10;
        }

        public QyA(QyA qyA, QyAComType comType, int Address = -1)
        {
            switch (comType)
            {
                case QyAComType.USART:
                    Com.WriteBytes = qyA.WriteUSART;
                    Com.ReadBytes = qyA.ReadUSART;
                    break;
                case QyAComType.SPI:
                    break;
                case QyAComType.I2C:
                    break;
            }
        }

        public string ReadSettingsDescription()
        {
            byte[] sendBytes = { 0xF0 };
            Com.WriteBytes(sendBytes, 0, 1);

            Thread.Sleep(200);

            byte[] result = new byte[1024];

            try
            {
                Com.ReadBytes(result, 0, result.Count());
            }
            catch
            {

            }

            return Encoding.Default.GetString(result);
        }

        public void WriteToDigitalOutputs(byte value)
        {
            byte[] sendBytes = { 0x20, value };
            Com.WriteBytes(sendBytes, 0, 2);
        }

        public void WriteToAnalogOutput(int Channel, int Value)
        {
            byte[] sendBytes = new byte[3];

            sendBytes[0] = (byte)(0x40 | Channel);
            sendBytes[1] = (byte)(Value >> 2);
            sendBytes[2] = (byte)((Value & 0x03) << 6);

            Com.WriteBytes(sendBytes, 0, 3);
        }

        Queue<byte> uBackBuffer = new Queue<byte>();
        public int ReadUSART(byte[] buffer, int offset, int count)
        {
            Stopwatch watchDog = Stopwatch.StartNew();

            byte[] readBuffer = new byte[33];
            do
            {
                Com.WriteBytes(new byte[] { 0x7F }, 0, 1);
                Thread.Sleep(1);

                try { Com.ReadBytes(readBuffer, 0, 33); } catch { }

                for (int i = 1; i < readBuffer[0]; i++)
                    uBackBuffer.Enqueue(readBuffer[i]);

            } while (uBackBuffer.Count() < count && watchDog.ElapsedMilliseconds < Timeout);

            for (int copyPnt = offset; copyPnt < (offset + count); copyPnt++)
            {
                if (uBackBuffer.Count == 0)
                    throw new Exception("Read Timeout");
                else
                    buffer[copyPnt] = uBackBuffer.Dequeue();
            }

            return count;
        }

        public void WriteUSART(byte[] buffer, int offset, int count)
        {
            int packetedCount = 0;
            int packetedIndex = 0;
            byte[] sendBuffer = new byte[(int)(count * 1.07) + 1];

            while (count - packetedIndex > 16)
            {
                sendBuffer[packetedCount++] = 0x6F;
                Array.Copy(buffer, offset + packetedIndex, 
                    sendBuffer, packetedCount, 16);

                packetedCount += 16;
                packetedIndex += 16;
            }

            byte leftOver = (byte)(count - packetedCount);
            if (leftOver > 0)
            {
                sendBuffer[packetedCount++] = (byte)(0x60 | leftOver - 1);
                Array.Copy(buffer, packetedIndex, sendBuffer, packetedCount, leftOver);
                packetedCount += leftOver;
            }

            Com.WriteBytes(sendBuffer, 0, packetedCount);
        }

        public QyAData QueryData(QyAReadFlags flags)
        {
            List<byte> sendData = new List<byte>();

            if (flags.HasFlag(QyAReadFlags.Digtals))
                sendData.Add(0x30);

            byte analogByte = (byte)(0x50 | ((byte)flags >> 1 & 0x0F));

            if (analogByte != 0x50)
                sendData.Add(analogByte);

            if (flags.HasFlag(QyAReadFlags.USART))
                sendData.Add(0x70);

            if(flags.HasFlag(QyAReadFlags.SPI))
                sendData.Add(0x90);

            if(flags.HasFlag(QyAReadFlags.I2C))
                sendData.Add(0xB0);

            byte[] readBytes = new byte[64];
            Com.WriteBytes(sendData.ToArray(), 0, sendData.Count);

            Thread.Sleep(10);
            try { Com.ReadBytes(readBytes, 0, readBytes.Count()); }
            catch { }

            QyAData readData = new QyAData
            {
                Analogs = new int[AnalogInputCount]
            };

            int index = 0;

            if (flags.HasFlag(QyAReadFlags.Digtals))
                readData.Digitals = readBytes[index++];

            for (int count = 0; count < AnalogInputCount; count++)
            {
                if ((analogByte & 0x01) == 1)
                {
                    readData.Analogs[count] = (readBytes[index++] << 2) +
                        (readBytes[index++] >> 6);
                }

                analogByte = (byte)(analogByte >> 1);
            }

            if (flags.HasFlag(QyAReadFlags.USART))
                index = UnPackage(readBytes, readData.USART, index);

            if (flags.HasFlag(QyAReadFlags.SPI))
                index = UnPackage(readBytes, readData.SPI, index);

            if (flags.HasFlag(QyAReadFlags.I2C))
                index = UnPackage(readBytes, readData.I2C, index);

            return readData;
        }

        private static int UnPackage(byte[] inputArray, byte[] outputArray, int readCount)
        {
            outputArray = new byte[inputArray[readCount]];
            Array.Copy(inputArray, readCount, outputArray, 0, outputArray.Count());
            readCount += outputArray.Count() + 1;
            return readCount;
        }

        public string GetBoardName()
        {
            return queryString(0x1D, 8);
        }

        public string GetSerialNumber()
        {
            return queryString(0x1E, 8);
        }

        public string GetFirmwareID()
        {
            return queryString(0x1F, 10).Trim();
        }

        public void WriteAllSettings(byte[] data)
        {
            Com.WriteBytes(new byte[] { 0xE7 }, 0, 1);
            Com.WriteBytes(data, 0, data.Length);
        }

        public bool CommitSettings()
        {
            Com.WriteBytes(new byte[] { 0xEE }, 0, 1);
            Thread.Sleep(10);

            byte[] result = new byte[1];
            return Com.ReadBytes(result, 0, 1) > 0;
        }

        public byte[] ReadAllSettings()
        {
            Com.WriteBytes(new byte[] { 0xFA }, 0, 1);
            Thread.Sleep(10);

            byte[] readBuffer = new byte[64];
            Com.ReadBytes(readBuffer, 0, 64);

            return readBuffer;
        }

        private string queryString(byte command, byte length)
        {
            Com.WriteBytes(new byte[] { command }, 0, 1);

            Thread.Sleep(5);
            byte[] readBuffer = new byte[length];
            Com.ReadBytes(readBuffer, 0, length);

            return Encoding.Default.GetString(readBuffer);

        }

    }

    [Flags]
    public enum QyAReadFlags : byte
    {
        Digtals = 1,
        Analog1 = 2,
        Analog2 = 4,
        Analog3 = 8,
        Analog4 = 16,
        USART = 32,
        SPI = 64,
        I2C = 128,
    }

    public enum QyAComType
    {
        USART,
        SPI,
        I2C
    }

    public struct QyAData
    {
        public int Digitals;
        public int[] Analogs;
        public byte[] USART;
        public byte[] SPI;
        public byte[] I2C;
    }


    public class ICom
    {
        public delegate string ReadStringDelegate();

        public ReadStringDelegate Read;

        public delegate string ReadToDelagate(string EndString);

        public ReadToDelagate ReadTo;

        public delegate int ReadByteDelegate(byte[] buffer, int offset, int count);

        public ReadByteDelegate ReadBytes;

        public delegate void WriteDelage(string Data);

        public WriteDelage Write;

        public delegate void WriteByteDelegate(byte[] buffer, int offset, int count);

        public WriteByteDelegate WriteBytes;
    }
}
